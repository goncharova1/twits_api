from flask import Flask, jsonify, request
from model.twit import Twit
import json

twits = []
app = Flask(__name__)


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Twit):
            return {'body': obj.body, 'author': obj.author}
        else:
            return super().default(obj)

app.json_encoder = CustomJSONEncoder

@app.route('/')
def ping():
    return jsonify({'name': ''})


@app.route('/twit', methods=['POST'])
def create_twit():
    """
    Функция создания твита
    {"body": "Hello World", "author": "@alicago"}
    """
    twit_json = request.get_json()
    twit = Twit(twit_json['body'], twit_json['author'])
    twits.append(twit)
    return jsonify({'status': 'success'})


@app.route('/twit', methods=['GET'])
def read_twit():
    """
    функция чтения списка твитов
    """
    return jsonify({'twits': twits})


@app.route('/twit/<int:index>', methods=['PUT'])
def update_twit(index):
    """
    Функция обновления твита. Принимает индекс твита, который мы меняем
    {"body": "New body", "author": "@new_author"}
    """
    twit_json = request.get_json()
    if index < 0 or index >= len(twits):
        return jsonify({'error': 'Invalid index'}), 400
    twit = twits[index]
    if 'body' in twit_json:
        twit.body = twit_json['body']
    if 'author' in twit_json:
        twit.author = twit_json['author']
    return jsonify({'status': 'success'})

@app.route('/twit/<int:index>', methods=['DELETE'])
def delete_twit(index):
    """
    Функция удаления твита. Принимает индекс твита, который мы хотим удалить.
    """
    if index < 0 or index >= len(twits):
        return jsonify({'error': 'Invalid index'}), 400
    del twits[index]
    return jsonify({'status': 'success'})

if __name__ == '__main__':
    app.run(debug=True)